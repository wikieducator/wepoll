WEpoll
====

WEpoll is an experimental way of conducting polls in
the wiki. It consists of a PHP extension that moderates
voting and a [widget](https://www.mediawiki.org/wiki/Extension:Widgets)
that both captures the votes and displays current results.

